# Arduino I²C slave for Duet

Create a slave that listen for command from I²C master (duet 2 wifi here)

## Wiring
|              |Arduino NANO |Duet 2 WIFI |
|--------------|-------------|------------|
|SDA           |A4           |TWD0        |
|SCL           |A5           |TWCK0       |
|5V            |5V           |+5V         |
|GND           |GND          |GND         |
|Buzzer        |D8           |            |
|NEO Pixel data|D9           |            |


## Usage

You can send command with M260 gcode command in RepRap firmware, [duet wiki](https://duet3d.dozuki.com/Wiki/M260).

### Reset
```gcode
M260 A8 B69
```
### Effects
```gcode
M260 A8 B70  ; breathing (toggle)
M260 A8 B71  ; chasing (toggle)
```
### Solid color
```gcode
M260 A8 B80  ; White (toggle)
M260 A8 B81  ; Red
M260 A8 B82  ; Green
M260 A8 B83  ; Blue
M260 A8 B84  ; Orange
```
### Song
```gcode
M260 A8 B90  ; Nokia rigntone
M260 A8 B91  ; Jurassic park theme song
M260 A8 B92  ; Pacman theme song
M260 A8 B93  ; Mortal kombat theme song
M260 A8 B94  ; Muse - new born
M260 A8 B95  ; Wellerman - Sea shanty
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
