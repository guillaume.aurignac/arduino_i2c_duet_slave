/*===================================================
                    Light Modes
  69: All OFF

  70: Breathing
  71: Chasing

  80: White
  81: Red
  82: Green
  83: Blue
  84: Orange

  90: Nokia
  91: Jurassic park
  92: Pacman
  93: Mortal kombat
  94: Muse new born
  95: Sea shanty
===================================================*/

#include <Wire.h>
#include <Adafruit_NeoPixel.h>
#include "pitches.h"

#define LED_PIN     9
#define LED_COUNT  33
#define BRIGHTNESS 255 //max = 255

// Declare our NeoPixel strip object:
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);

int BUZ_PIN = 8;

int flag = 71;
int mode = 71;
int prev_mode = 71;

bool breathing = false;
int b_step = 0;
unsigned long b_last_step;

bool chasing = false;
bool chase_bckw = false;
int c_step = 0;
unsigned long c_last_step;

bool light_state = false;


int nokia[] = {
  // Nokia Ringtone 
  NOTE_E5, 8, NOTE_D5, 8, NOTE_FS4, 4, NOTE_GS4, 4, 
  NOTE_CS5, 8, NOTE_B4, 8, NOTE_D4, 4, NOTE_E4, 4, 
  NOTE_B4, 8, NOTE_A4, 8, NOTE_CS4, 4, NOTE_E4, 4,
  NOTE_A4, 2, 
};
int notes_nokia = sizeof(nokia) / sizeof(nokia[0]) / 2;

int jurassic[] = {
  // jurassic park theme song
  // https://musescore.com/user/26033266/scores/5211161
  NOTE_C4, -2, NOTE_C4, 8, NOTE_B3, 8, NOTE_C4, -2, 
  NOTE_C4, 8, NOTE_B3, 8, NOTE_C4, -4, NOTE_D4, 8, 
  NOTE_D4, -4, NOTE_F4, 8, NOTE_F4, -2, NOTE_E4, 8,
  NOTE_C4, 8, NOTE_D4, -4, NOTE_B3, 8, NOTE_G3, 4,
  NOTE_E4, 8, NOTE_C4, 8, NOTE_D4, -2, NOTE_G4, 8,
  NOTE_C4, 8, NOTE_F4, -4, NOTE_E4, 8, NOTE_E4, -4,
  NOTE_D4, 8, NOTE_D4, -2, NOTE_C4, 8, NOTE_B3, 8,
  
  NOTE_C4, -2, NOTE_C4, 8, NOTE_B3, 8, NOTE_C4, -2, 
  NOTE_C4, 8, NOTE_B3, 8, NOTE_C4, -4, NOTE_D4, 8, 
  NOTE_D4, -4, NOTE_F4, 8, NOTE_F4, -2, NOTE_E4, 8,
  NOTE_C4, 8, NOTE_D4, -4, NOTE_B3, 8, NOTE_G3, 4,
  NOTE_E4, 8, NOTE_C4, 8, NOTE_D4, -2, NOTE_G4, 8,
  NOTE_C4, 8, NOTE_F4, -4, NOTE_E4, 8, NOTE_E4, -4,
  NOTE_D4, 8, NOTE_D4, -2
};
int notes_jurassic = sizeof(jurassic) / sizeof(jurassic[0]) / 2;

int pacman[] = {
  // Pacman
  // https://musescore.com/user/85429/scores/107109
  NOTE_B4, 16, NOTE_B5, 16, NOTE_FS5, 16, NOTE_DS5, 16, //1
  NOTE_B5, 32, NOTE_FS5, -16, NOTE_DS5, 8, NOTE_C5, 16,
  NOTE_C6, 16, NOTE_G5, 16, NOTE_E5, 16, NOTE_C6, 32, NOTE_G5, -16, NOTE_E5, 8,

  NOTE_B4, 16,  NOTE_B5, 16,  NOTE_FS5, 16,   NOTE_DS5, 16,  NOTE_B5, 32,  //2
  NOTE_FS5, -16, NOTE_DS5, 8,  NOTE_DS5, 32, NOTE_E5, 32,  NOTE_F5, 16,
  NOTE_F5, 32,  NOTE_FS5, 32,  NOTE_G5, 16,  NOTE_G5, 32, NOTE_GS5, 32,  NOTE_A5, 16, NOTE_B5, 8
};
int notes_pacman = sizeof(pacman) / sizeof(pacman[0]) / 2;

int mortal[] = {
  // mortal kombat theme
  // https://musescore.com/user/1234751/scores/950851/piano-tutorial
  NOTE_A4, 8, NOTE_A4, 8, NOTE_C5, 8, NOTE_A4, 8,
  NOTE_D5, 8, NOTE_A4, 8, NOTE_E5, 8, NOTE_D5, 8,

  NOTE_C5, 8, NOTE_C5, 8, NOTE_E5, 8, NOTE_C5, 8,
  NOTE_G5, 8, NOTE_C5, 8, NOTE_E5, 8, NOTE_C5, 8,

  NOTE_G4, 8, NOTE_G4, 8, NOTE_B4, 8, NOTE_G4, 8,
  NOTE_C5, 8, NOTE_G4, 8, NOTE_D5, 8, NOTE_C5, 8,

  NOTE_F4, 8, NOTE_F4, 8, NOTE_A4, 8, NOTE_F4, 8,
  NOTE_C5, 8, NOTE_F4, 8, NOTE_C5, 8, NOTE_B4, 8, NOTE_A4, 8
};
int notes_mortal = sizeof(mortal) / sizeof(mortal[0]) / 2;

int new_born[] = {
  // Muse new born
  // https://musescore.com/user/3220536/scores/5129114
  NOTE_B5, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  NOTE_B5, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  
  NOTE_B5, 8, NOTE_FS5, 8, NOTE_DS5, 8, NOTE_FS5, 8,
  NOTE_B5, 8, NOTE_FS5, 8, NOTE_DS5, 8, NOTE_FS5, 8,
  
  NOTE_B5, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  NOTE_B5, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  
  NOTE_B5, 8, NOTE_FS5, 8, NOTE_DS5, 8, NOTE_FS5, 8,
  NOTE_B5, 8, NOTE_FS5, 8, NOTE_DS5, 8, NOTE_FS5, 8,
  
  NOTE_B5, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  NOTE_B5, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  
  NOTE_C6, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  NOTE_C6, 8, NOTE_G5, 8, NOTE_E5, 8, NOTE_G5, 8,
  
  NOTE_B5, 8, NOTE_G5, 8, NOTE_D5, 8, NOTE_G5, 8,
  NOTE_B5, 8, NOTE_G5, 8, NOTE_D5, 8, NOTE_G5, 8,
  
  NOTE_B5, 8, NOTE_FS5, 8, NOTE_DS5, 8, NOTE_FS5, 8,
  NOTE_B5, 8, NOTE_FS5, 8, NOTE_DS5, 8,
  
  NOTE_E2, 8, NOTE_E2, 8, NOTE_E2, 8, NOTE_E2, 8,
  NOTE_G2, 8, NOTE_E2, 8, NOTE_G2, 8, NOTE_GS2, 8,
};
int notes_new_born = sizeof(new_born) / sizeof(new_born[0]) / 2;

int new_born_bass[] = {
  // Muse new born bass part
  // https://musescore.com/user/3220536/scores/5129114
  NOTE_E3, 8, NOTE_E3, 8, NOTE_E3, 8, NOTE_E3, 8,
  NOTE_G3, 8, NOTE_E3, 8, NOTE_G3, 8, NOTE_GS3, 8,
  
  NOTE_A3, 8, NOTE_A3, 8, NOTE_A3, 8, NOTE_A3, 8,
  NOTE_C4, 8, NOTE_A3, 8, NOTE_C4, 8, NOTE_CS4, 8,
  
  NOTE_D4, 8, NOTE_D3, 8, NOTE_D3, 8, NOTE_D4, 8,
  NOTE_D3, 8, NOTE_D3, 8, NOTE_E3, 8, NOTE_FS3, 8,
  
  NOTE_G3, 8, NOTE_G3, 8, NOTE_G3, 8, NOTE_G3, 8,
  NOTE_B3, 8, NOTE_B3, 8, NOTE_B3, 8, NOTE_B3, 8,
};
int notes_new_born_bass = sizeof(new_born_bass) / sizeof(new_born_bass[0]) / 2;

int sea_shanty[] = {
  // Wellerman Sea shanty
  // https://musescore.com/user/35843650/scores/6583881
  NOTE_G4, 2,
  NOTE_C4, 4, NOTE_C4, 8, NOTE_C4, 8, NOTE_C4, 4, NOTE_DS4, 4,
  NOTE_G4, 4, NOTE_G4, 4, NOTE_G4, -4, NOTE_G4, 8,
  NOTE_GS4, 4, NOTE_F4, 8, NOTE_F4, 8, NOTE_F4, 4, NOTE_F4, 8, NOTE_GS4, 8,

  NOTE_C5, 8, NOTE_C5, 8, NOTE_G4, 4, NOTE_G4, -4, NOTE_G4, 8,
  NOTE_C4, 4, NOTE_C4, 4, NOTE_C4, 4, NOTE_DS4, 4,
  NOTE_G4, 4, NOTE_G4, 4, NOTE_G4, -4, NOTE_G4, 8,
  
  NOTE_F4, 4, NOTE_F4, 4, NOTE_DS4, 8, NOTE_DS4, 8, NOTE_D4, 4,
  NOTE_C4, 1,
};
int notes_sea_shanty = sizeof(sea_shanty) / sizeof(sea_shanty[0]) / 2;

void setup() {
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(BRIGHTNESS);
  
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  
  pinMode(BUZ_PIN, OUTPUT);

  startup();
}

void loop() {
  switch (flag) {
    case 69:
      // All off
      breathing = false;
      chasing = false;
      light_state = false;

      strip.setBrightness(BRIGHTNESS);
      strip.fill(strip.Color(0,0,0));
      strip.show();
      
      flag = 0;
      mode = 69;
      prev_mode = 69;
      break;

    // =================== Animation ================
      
    case 70:
      // breathing mode
      breathing = !breathing;
      chasing = false;
      light_state = false;

      if (breathing) {
        prev_mode = mode;
        mode = 70;
        flag = 0;
      } else {
        mode = prev_mode;
        flag = mode;
      }
      
      b_last_step = millis();
      break;
      
    case 71:
      // Chasing mode
      breathing = false;
      chasing = !chasing;
      light_state = false;

      if (chasing) {
        prev_mode = mode;
        mode = 71;
        flag = 0;
      } else {
        mode = prev_mode;
        flag = mode;
      }
      c_last_step = millis();
      break;

    // ================== Solid Light ===============
      
    case 80:
      // White light
      breathing = false;
      chasing = false;
      light_state = !light_state;

      if (light_state) {
        prev_mode = mode;
        mode = 80;
        flag = 0;
      } else {
        mode = prev_mode;
        flag = mode;
      }
      light();
      break;
      
    case 81:
      // Red light
      breathing = false;
      light_state = false;
      chasing = false;

      strip.setBrightness(BRIGHTNESS);
      strip.fill(strip.Color(255,0,0));
      strip.show();
      
      flag = 0;
      mode = 81;
      break;
      
    case 82:
      // Green light
      breathing = false;
      light_state = false;
      chasing = false;

      strip.setBrightness(BRIGHTNESS);
      strip.fill(strip.Color(0,255,0));
      strip.show();
      
      flag = 0;
      mode = 82;
      break;
      
    case 83:
      // Blue light
      breathing = false;
      light_state = false;
      chasing = false;

      strip.setBrightness(BRIGHTNESS);
      strip.fill(strip.Color(0,0,255));
      strip.show();
      
      flag = 0;
      mode = 83;
      break;
      
    case 84:
      // Orange light
      breathing = false;
      light_state = false;
      chasing = false;

      strip.setBrightness(BRIGHTNESS);
      strip.fill(strip.Color(255,128,0));
      strip.show();
      
      flag = 0;
      mode = 84;
      break;

    // ================== Song ==================
      
    case 90:
      // Nokia song
      player(nokia, notes_nokia, 180);
      
      flag = 0;
      break;
      
    case 91:
      // Jurassic park song
      player(jurassic, notes_jurassic, 102);
      
      flag = 0;
      break;
      
    case 92:
      // pacman song
      player(pacman, notes_pacman, 105);
      
      flag = 0;
      break;
      
    case 93:
      // mortal kombat song
      player(mortal, notes_mortal, 125);
      
      flag = 0;
      break;
      
    case 94:
      // new born song
      player(new_born, notes_new_born, 146);
      player(new_born_bass, notes_new_born_bass, 154);
      
      flag = 0;
      break;
      
    case 95:
      // sea shanty song
      player(sea_shanty, notes_sea_shanty, 192);
      
      flag = 0;
      break;
      
    default:
      break;
  }

  if (breathing) {b_step = breath(b_step);}
  if (chasing) {c_step = chaser(c_step);}
}

void receiveEvent(int howMany)
{
  // receive byte as an integer
  flag = Wire.read();
}



void startup() {
  uint32_t color = strip.Color(100, 0, 255); //purple
  int wait = 20;
  tone(BUZ_PIN, 2000);
  delay(80);
  noTone(BUZ_PIN);
  delay(80);

  for(int i=0; i<(strip.numPixels()/2)+1; i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.setPixelColor(strip.numPixels()-i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }

  delay(100);
  
  strip.clear();
  strip.show();

  delay(200);

  for (int i = 0; i < 3; i++) {
    strip.fill(color);
    strip.show();
    tone(BUZ_PIN, 2500);
    delay(80);
  
    strip.clear();
    strip.show();
    noTone(BUZ_PIN);
    delay(80);
  }
}

int breath(int b_step) {

  if (millis()-b_last_step > 20) {
    strip.setBrightness(BRIGHTNESS);
    if (b_step > 0) {
      for (int i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, int(100*b_step/100), 0, int(255*b_step/100));
      }
      strip.show();
      
    } else {
      for (int i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, int(100*abs(b_step)/100), 0, int(255*abs(b_step)/100));
      }
      strip.show();
    }
    
    b_step++;
  
    if (b_step > 100) {b_step = -100;}
    b_last_step = millis();
  }

  return b_step;
}

void light() {
  if (light_state) {
    tone(BUZ_PIN, 2000);
    strip.setBrightness(255);
    strip.fill(strip.Color(255,255,255));
    strip.show();
    delay(80);
    
    noTone(BUZ_PIN);
    delay(80);
    
    tone(BUZ_PIN, 2500);
    delay(80);
    noTone(BUZ_PIN);
  } else {
    tone(BUZ_PIN, 2500);
    //strip.setBrightness(BRIGHTNESS);
    //strip.fill(strip.Color(0,0,0));
    //strip.show();
    delay(80);
    
    noTone(BUZ_PIN);
    delay(80);
    
    tone(BUZ_PIN, 2000);
    delay(80);
    noTone(BUZ_PIN);
  }
}

int chaser(int c_step) {
  int trail_len = 6;
  int min_inten = 8;
  float coef;
  
  if (millis()-c_last_step > 100) {
    strip.setBrightness(BRIGHTNESS);

    //center pixel + off the rest
    for (int i = 0; i < strip.numPixels(); i++) {
      if (i == c_step) {
        strip.setPixelColor(i, 100, 0, 255); // Purple
        
      } else if (c_step-i > 0 && c_step-i <= trail_len) {
        coef = 1 - ( (c_step-i) * (100-min_inten)/trail_len/100.0 );
        strip.setPixelColor(i, int(100*coef), 0, int(255*coef));

      } else if (i-c_step > 0 && i-c_step <= trail_len) {
        coef = 1 - ( (i-c_step) * (100-min_inten)/trail_len/100.0 );
        strip.setPixelColor(i, int(100*coef), 0, int(255*coef));

      } else {
        coef = min_inten / 100.0;
        strip.setPixelColor(i, int(100*coef), int(0*coef), int(255*coef));
      }
    }

    strip.show();

    if (chase_bckw) {
      c_step--;
    } else {
      c_step++;
    }
    
    if (c_step >= strip.numPixels()) {chase_bckw = !chase_bckw;}

    c_last_step = millis();
  }
  return c_step;
}

void player(int melody[], int notes, int tempo) {
    
  int wholenote = (60000 * 4) / tempo;
  int divider = 0, noteDuration = 0;

  for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {

    // calculates the duration of each note
    divider = melody[thisNote + 1];
    if (divider > 0) {
      // regular note, just proceed
      noteDuration = (wholenote) / divider;
    } else if (divider < 0) {
      // dotted notes are represented with negative durations!!
      noteDuration = (wholenote) / abs(divider);
      noteDuration *= 1.5; // increases the duration in half for dotted notes
    }

    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(BUZ_PIN, melody[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    delay(noteDuration);

    // stop the waveform generation before the next note.
    noTone(BUZ_PIN);
  }
}
